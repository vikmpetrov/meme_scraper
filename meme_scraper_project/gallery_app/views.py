from django.shortcuts import render
from scraper_app.models import Image


def home(request):
    return render(request, 'index.html', context={'image': Image.random()})
