from django.apps import AppConfig
from django.db.utils import OperationalError
from django.core.exceptions import AppRegistryNotReady
from .log import logger

LOGGER = logger('Scraper App Config')


class ScraperConfig(AppConfig):
    name = 'scraper_app'
    verbose_name = 'Scraper App'

    def ready(self):
        try:
            from . import models
        except AppRegistryNotReady as error:
            return self._whoops(error)
        try:
            self._website_record_check(name='memebase.cheezburger', url='memebase.cheezburger.com')
            self._website_record_check(name='cheezburger', url='cheezburger.com')
        except OperationalError as error:
            return self._whoops(error)
        return LOGGER.debug('App configuration - OK.')

    @staticmethod
    def _website_record_check(name, url):
        from .models import Website
        if not Website.objects.filter(name=name):
            memebase = Website(name=name, url=url)
            memebase.save()
            LOGGER.debug('Recorded website: %s' % url)

    @staticmethod
    def _whoops(error):
        LOGGER.error(error)
        LOGGER.warn('Database check was unsuccessful.')
        return LOGGER.error('App configuration - Failed.')
