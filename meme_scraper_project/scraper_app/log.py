# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import logging


def logger(name):
    """
    Create a logger object to log the activity of some module.
    :param name: the name given to the logger object;
    :return: a logger object;
    """
    logger = logging.getLogger(name)

    # Format the logger's output:
    log_formatter = logging.Formatter("%(asctime)s [%(name)s] [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")

    # Create a console handler for the logger's output:
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    logger.addHandler(console_handler)
    logger.setLevel(logging.DEBUG)
    logger.info('Starting...')
    return logger
