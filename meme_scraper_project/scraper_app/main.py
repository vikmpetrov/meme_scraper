# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import json
from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
from .models import Website, Meme, Image
from .log import logger
from .utils import url_cleanup, image_from_url, image_histogram, json_to_string

LOGGER = logger(name='Meme Scraper')


class MemeScraper:
    """Class for scraping a website and collecting meme data."""

    def __init__(self):
        self.data = dict()
        self.website_process = {'memebase.cheezburger': self.process_cheezburger,
                                'cheezburger': self.process_cheezburger}

    def get_content(self, url):
        """
        Attempts to get text content from an HTTP GET request.
        If the content-type of response is some kind of HTML/XML, return the
        text content, otherwise return None.
        :param url: the target URL;
        :return: Text content or None.
        """
        try:
            with closing(get(url, stream=True)) as resp:
                if self.response_ok(resp):
                    return resp.content
                else:
                    return None

        except RequestException as e:
            LOGGER.error('Error during requests to {0} : {1}'.format(url, str(e)))
            return None

    @staticmethod
    def response_ok(resp):
        """
        Returns True if the response is HTML, False otherwise.
        :param resp: a requests response;
        :return: True or False.
        """
        content_type = resp.headers['Content-Type'].lower()
        return (resp.status_code == 200
                and content_type is not None
                and content_type.find('html') > -1)

    def scrape(self, url, pages=10):
        """
        Scrape a given URL for meme data.
        :param url: the URL to start scraping from;
        :param pages: the number of web pages to scrape;
        :return: True if there's a valid response, otherwise False.
        """
        website = self.get_website_entity(url=url)
        html = self.get_response_html(url=url)
        if html:
            self.website_process[website.name](website=website, html=html, pages=pages)
            return True
        return False

    @staticmethod
    def get_website_entity(url):
        url = url_cleanup(url=url)
        LOGGER.debug(msg='Scraping target URL: %s' % url)
        url_root = url.split('/')[0]
        LOGGER.debug(msg='Scraping URL root: %s' % url_root)
        query = Website.objects.filter(url=url_root)
        if query:
            website = query.first()
            LOGGER.debug('Found matching website: %s' % website.name)
        else:
            website = Website(url=url_root, name=url.rsplit('.', 1)[0])
            website.save()
            LOGGER.debug('Recorded new website: %s' % website.name)
        return website

    def get_response_html(self, url):
        response = self.get_content(url)
        if response:
            LOGGER.debug(msg='Valid response received from URL: %s' % url)
            return BeautifulSoup(response, 'html.parser')
        LOGGER.warn(msg='Invalid response received from URL: %s' % url)
        return None

    def process_cheezburger(self, website, html, pages=10):
        content_divs = [div for div in html.find_all('div', {"class": "nw-post js-post"})]
        for div in content_divs:
            data = json.loads(div.get('data-model'))
            if not data['AssetType']:
                self.record_meme(data=data, website=website)
            else:
                child_html = self.get_response_html(url=data['CanonicalUrl'])
                if child_html:
                    self.process_cheezburger(website=self.get_website_entity(url=data['CanonicalUrl']),
                                             html=child_html,
                                             pages=1)
        for page in range(pages):
            if page:
                page_url = 'http://{0}/page/{1}'.format(website.url, page)
                LOGGER.debug(msg='Processing website page: %s' % page_url)
                page_html = self.get_response_html(url=page_url)
                if page_html:
                    self.process_cheezburger(website=website,
                                             html=page_html,
                                             pages=1)

    @staticmethod
    def record_meme(data, website):
        image_data = image_from_url(url=data['Url'])
        histogram = json_to_string(image_histogram(image=image_data))
        # Compare image URLs and histograms to avoid re-posts:
        if not Image.objects.filter(url=data['Url']).filter(histogram=histogram):
            meme = Meme(title=data['Title'], url=data['CanonicalUrl'], website=website, src=json_to_string(data))
            meme.save()
            LOGGER.debug('Recorded new meme: "%s" @ %s' % (meme.title, meme.url))
            image = Image(url=data['Url'], width=data['Width'], height=data['Height'], meme=meme, histogram=histogram)
            image.save()
            LOGGER.debug('Recorded new image: %s for meme "%s"' % (image.url, meme.title))
