from django.core.management.base import BaseCommand, CommandError
from scraper_app.main import MemeScraper


class Command(BaseCommand):
    help = 'Command line website scraping.'
    scraper = MemeScraper()

    def add_arguments(self, parser):
        parser.add_argument('url', type=str)

    def handle(self, *args, **options):
        self.scraper.scrape(url=options['url'])
