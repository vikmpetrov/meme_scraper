import random
from django.db import models
from django.utils import timezone
from django.db.models import Max
from .utils import url_cleanup
from .utils import string_to_json, json_to_string


class MethodsMixin(object):

    @classmethod
    def random(cls):
        max_id = cls.objects.all().aggregate(max_id=Max('id'))['max_id']
        if not max_id:
            return None
        while True:
            pk = random.randint(1, max_id)
            result = cls.objects.filter(pk=pk).first()
            if result:
                return result


class Website(models.Model, MethodsMixin):
    name = models.CharField(max_length=250)
    url = models.TextField()
    created = models.DateTimeField(default=timezone.now())
    meta = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.url = url_cleanup(self.url)
        super().save(*args, **kwargs)


class Meme(models.Model, MethodsMixin):
    title = models.TextField()
    url = models.TextField()
    website = models.ForeignKey(Website, on_delete=models.CASCADE)
    created = models.DateTimeField(default=timezone.now())
    meta = models.TextField(null=True, blank=True)
    src = models.TextField(null=True, blank=True)
    rating = models.IntegerField(default=0)
    flagged = models.BooleanField(default=False)

    @property
    def images(self):
        return self.image_set.all()

    @property
    def primary_image(self):
        images = self.images
        if images:
            return images.first()


class Image(models.Model, MethodsMixin):
    url = models.TextField()
    width = models.IntegerField()
    height = models.IntegerField()
    created = models.DateTimeField(default=timezone.now())
    meme = models.ForeignKey(Meme, on_delete=models.CASCADE)
    meta = models.TextField(null=True, blank=True)
    histogram = models.TextField()

    def save(self, *args, **kwargs):
        self.histogram = json_to_string(self.histogram)
        super().save(*args, **kwargs)

    @property
    def histogram_value(self):
        return string_to_json(self.histogram)
