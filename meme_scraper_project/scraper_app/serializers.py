from rest_framework import serializers
from .models import Website, Meme, Image


class WebsiteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Website
        fields = ('id', 'name', 'url', 'created', 'meta')


class MemeSerializer(serializers.HyperlinkedModelSerializer):
    website = WebsiteSerializer(read_only=True)

    class Meta:
        model = Meme
        fields = ('id', 'title', 'url', 'website', 'created', 'meta')


class ImageSerializer(serializers.HyperlinkedModelSerializer):
    meme = MemeSerializer(read_only=True)

    class Meta:
        model = Image
        fields = ('id', 'url', 'width', 'height', 'meme', 'created', 'meta')
