# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from django.test import TestCase
from .main import MemeScraper
from .utils import image_histogram


class MemebaseTest(TestCase):
    scraper = MemeScraper()

    def setUp(self):
        pass

    def test(self):
        self.assertTrue(self.scraper.scrape(url='https://memebase.cheezburger.com/', pages=1))


class ImageComparisonTest(TestCase):
    images_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'static', 'images')
    test_a = os.path.join(images_dir, 'test_meme_a.jpg')  # similar to test_b;
    test_b = os.path.join(images_dir, 'test_meme_b.jpg')  # similar to test_a;
    test_c = os.path.join(images_dir, 'test_meme_c.jpg')  # different to test_a and test_b;
    test_d = os.path.join(images_dir, 'test_meme_d.jpg')  # identical to test_c;

    def setUp(self):
        pass

    def test(self):
        self.assertNotEquals(image_histogram(image=self.test_a), image_histogram(image=self.test_b))
        self.assertNotEquals(image_histogram(image=self.test_b), image_histogram(image=self.test_c))
        self.assertEqual(image_histogram(image=self.test_c), image_histogram(image=self.test_d))
