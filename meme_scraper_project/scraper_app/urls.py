from django.urls import path, include
from django.conf.urls import url
from . import views


# RESTful patterns:
urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/<str:entity_model>/', views.entity),
    path('api/<str:entity_model>/<int:entity_id>/', views.entity),
    path('api/<str:entity_model>/random/', views.random_entity),
]


# Update patterns:
urlpatterns += [
    path('meme/<int:meme_id>/favourite/', views.favourite_meme),
    path('meme/<int:meme_id>/upvote/', views.upvote_meme),
    path('meme/<int:meme_id>/downvote/', views.downvote_meme),
]
