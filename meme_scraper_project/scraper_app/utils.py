import requests
import json
import operator
import math
from functools import reduce
from PIL import Image


def url_cleanup(url):
    return url.replace('www.', '').replace('https://', '').replace('http://', '')


def image_from_url(url):
    response = requests.get(url, stream=True)
    if response:
        return response.raw
    return None


def image_histogram(image):
    return Image.open(image).histogram()


def compare_images(image_a, image_b):
    histogram_a = image_histogram(image_a)
    histogram_b = image_histogram(image_b)
    return math.sqrt(reduce(operator.add, map(lambda a, b: (a - b) ** 2, histogram_a, histogram_b)) / len(histogram_a))


def json_to_string(content):
    """
    Convert a dictionary object to a string.
    :param content: the content to convert to a string;
    :return: a string representation of a dictionary;
    """
    return json.dumps(content)


def string_to_json(string):
    """
    Convert a string to a dictionary.
    :param string: the string to convert;
    :return: a dictionary;
    """
    try:
        return json.loads(string.decode())
    except AttributeError:
        return json.loads(str(string))
