from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import WebsiteSerializer, MemeSerializer, ImageSerializer
from .models import Website, Meme, Image


MODEL_SERIALIZER = {
    'Website': WebsiteSerializer,
    'Meme': MemeSerializer,
    'Image': ImageSerializer
}


def _sanity_check_model_name(model):
    if not model[0].isupper():
        model = model.title()
    return model


@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def entity(request, entity_model, entity_id=None):
    entity_model = _sanity_check_model_name(entity_model)
    try:
        model = eval(entity_model)
    except NameError:
        return Response({'error': 'Invalid entity type.'}, status=status.HTTP_400_BAD_REQUEST)

    if entity_id:
        try:
            instance = model.objects.get(id=entity_id)
        except model.DoesNotExist:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        if entity_id:
            result = model.objects.get(id=entity_id)
            serializer = MODEL_SERIALIZER[entity_model](result)
        else:
            result = model.objects.all()
            serializer = MODEL_SERIALIZER[entity_model](result, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = MODEL_SERIALIZER[entity_model](data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'PUT':
        if entity_id:
            serializer = MODEL_SERIALIZER[entity_model](instance, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        if entity_id:
            instance.delete()
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response({}, status=status.HTTP_204_NO_CONTENT)

    return Response({}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def random_entity(request, entity_model):
    if request.method == 'GET':
        entity_model = _sanity_check_model_name(entity_model)
        try:
            model = eval(entity_model)
        except NameError:
            return Response({'error': 'Invalid entity type.'}, status=status.HTTP_400_BAD_REQUEST)
        result = model.random()
        serializer = MODEL_SERIALIZER[entity_model](result)
        return Response(serializer.data)


def favourite_meme(request, meme_id):
    if request.method == 'POST':
        meme = Meme.objects.get(id=meme_id)
        request.user.favourites.add(meme)
        request.user.save()


def upvote_meme(request, meme_id):
    if request.method == 'POST':
        meme = Meme.objects.get(id=meme_id)
        meme.rating += 1
        meme.save()
        request.user.upvoted.add(meme)
        request.user.save()


def downvote_meme(request, meme_id):
    if request.method == 'POST':
        meme = Meme.objects.get(id=meme_id)
        meme.rating -= 1
        meme.save()
        request.user.downvoted.add(meme)
        request.user.save()
