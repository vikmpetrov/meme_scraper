var meme_history = [];
var current_meme_id;

$('#button-next').on('click', function(event) {
    return next_meme();
});

function next_meme() {
    $.ajax({
        url: '/api/image/random/',
        type: 'GET',
        success: function(json) {
            meme_history.push(current_meme_id);
            load_meme(json);
            current_meme_id = json['id'];
        }
    })
}

function load_meme(json) {
    $('#meme-image').fadeOut(function() {
        $('#loading-spinner').fadeIn(function() {
            $('#meme-image').attr('src', json['url']);
        });
    });
    $('#meme-title').fadeOut(function() {
        $('#meme-title').text(json['meme']['title']).attr('href', json['meme']['url']).fadeIn();
    });
    $('#meme-source').fadeOut(function() {
        $('#meme-source').text('http://' + json['meme']['website']['url']).attr('href', json['meme']['website']['url']).fadeIn();
    });
}

$('#meme-image').on('load', function(event) {
    $(this).fadeIn();
    $('#loading-spinner').fadeOut();
});

function previous_meme() {
    var meme_history_length = meme_history.length;
    if (meme_history_length != 0) {
        var previous_meme_id = meme_history[meme_history_length-1]
        $.ajax({
            url: '/api/image/' + previous_meme_id,
            type: 'GET',
            success: function(json) {
                load_meme(json);
                if (meme_history_length > 1) {
                    meme_history.splice(-1,1)
                }
            }
        })
    }
}

$('#button-previous').on('click', function(event) {
    return previous_meme();
});

$(document).keydown(function(e){
    if (e.keyCode == 39) {
        next_meme();
    } else if (e.keyCode == 37) {
        previous_meme();
    }
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$('#button-favourite').on('click', function(event) {
    $.ajax({
        url: '/favourite/' + current_meme_id,
        type: 'POST',
        data: {'id': current_meme_id, csrfmiddlewaretoken: getCookie('csrftoken')}
    })
});

$('#button-favourite').on('click', function(event) {
    $.ajax({
        url: 'meme/' + current_meme_id + '/favourite/',
        type: 'POST',
        data: {'id': current_meme_id, csrfmiddlewaretoken: getCookie('csrftoken')}
    })
});

$('#button-upvote').on('click', function(event) {
    $.ajax({
        url: 'meme/' + current_meme_id + '/upvote/',
        type: 'POST',
        data: {'id': current_meme_id, csrfmiddlewaretoken: getCookie('csrftoken')}
    })
});

$('#button-downvote').on('click', function(event) {
    $.ajax({
        url: 'meme/' + current_meme_id + '/downvote/',
        type: 'POST',
        data: {'id': current_meme_id, csrfmiddlewaretoken: getCookie('csrftoken')}
    })
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
