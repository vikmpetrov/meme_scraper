from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from scraper_app.models import Meme


class CustomUserManager(UserManager):
    pass


class User(AbstractUser):
    objects = CustomUserManager()
    upvoted = models.ManyToManyField(Meme, related_name='upvoted_memes')
    downvoted = models.ManyToManyField(Meme, related_name='downvoted_memes')
    favourites = models.ManyToManyField(Meme, related_name='favourite_memes')
